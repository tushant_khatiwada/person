from django.shortcuts import render, redirect
from django.forms.models import model_to_dict
from django.http import JsonResponse
from allauth.account.forms import LoginForm

from .models import Cart
from orders.models import Order
from billing.models import BillingProfile
from furnitures.models import Furniture
from accounts.models import GuestEmail
from accounts.forms import GuestForm
from addresses.forms import AddressForm, AddressCheckoutForm
from addresses.models import Address

def cart(request):
	cart_obj, new_obj = Cart.objects.new_or_get(request)
	login_form = LoginForm(request=request)
	guest_form = GuestForm(request=request)
	context = {
		"cart": cart_obj,
		"login_form": login_form,
		"guest_form": guest_form,
	}
	return render(request, 'carts/cart.html', context)

def cart_detail_api_view(request):
	cart_obj, new_obj = Cart.objects.new_or_get(request)
	products = [{
			"id": x.id,
			"url": x.get_absolute_url(),
			"name": x.name,
			"price": x.price,
			"quantity": x.quantity,
			"image": x.first_image(),
			"furniture_total_price": x.furniture_total_price(),
			}
			for x in cart_obj.furnitures.all()]
	cart_data  = {"products": products, "subtotal": cart_obj.sub_total, "total": cart_obj.total}
	return JsonResponse(cart_data)

def cart_update(request):
	furniture_id = request.POST.get('furniture_id')
	if furniture_id is not None:
		try:
			furniture_obj = Furniture.objects.get(id=furniture_id)
		except Furniture.DoesNotExist:
			return redirect("carts")
		cart_obj, new_obj = Cart.objects.new_or_get(request)
		if furniture_obj in cart_obj.furnitures.all():
			cart_obj.furnitures.remove(furniture_obj)
			added = False
		else:
			cart_obj.furnitures.add(furniture_obj)
			added = True
		request.session['cart'] = cart_obj.furnitures
		request.session['cart_items'] = cart_obj.furnitures.count()
		if request.is_ajax():
			json_data = {
				"added": added,
				"removed": not added,
				"cartItemCount": cart_obj.furnitures.count()
			}
			return JsonResponse(json_data, status=200)
	return redirect("carts")

def checkout(request):
	cart_obj, cart_created = Cart.objects.new_or_get(request)
	order_obj = None
	if cart_created or cart_obj.furnitures.count() == 0:
		return redirect('carts')
	login_form = LoginForm(request=request)
	guest_form = GuestForm(request=request)
	address_form = AddressCheckoutForm()
	guest_email_id = request.session.get('guest_email_id')
	billing_address_id = request.session.get("billing_address_id", None)
	shipping_address_id = request.session.get("shipping_address_id", None)
	billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
	if billing_profile is not None:
		order_obj, order_obj_created = Order.objects.new_or_get(billing_profile, cart_obj)
		if shipping_address_id:
			order_obj.shipping_address = Address.objects.get(id=shipping_address_id)
			del request.session["shipping_address_id"]
		if billing_address_id:
			order_obj.billing_address = Address.objects.get(id=billing_address_id)
			del request.session["billing_address_id"]
		if billing_address_id or shipping_address_id:
			order_obj.save()
	context = {
		"order": order_obj,
		"login_form": login_form,
		"guest_form": guest_form,
		"address_form": address_form,
		"billing_profile": billing_profile
	}
	return render(request, "carts/checkout.html", context)

def checkout_billing(request):
	address_form = AddressCheckoutForm()
	return render(request, "carts/checkout_billing.html", context)

def checkout_shipping(request):
	address_form = AddressCheckoutForm()
	return render(request, "carts/checkout_shipping.html", context)

def checkout_complete(request):
	context = {}
	return render(request, 'carts/checkout_complete.html', context)
