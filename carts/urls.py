from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.cart, name="carts"),
    url(r'^api/refresh$', views.cart_detail_api_view, name="cart-api"),
    url(r'^checkout$', views.checkout, name="checkout"),
    url(r'^checkout/success$', views.checkout_complete, name="checkout-success"),
    url(r'^update$', views.cart_update, name="update"),
]
