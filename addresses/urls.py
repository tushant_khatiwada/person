from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^checkout/address/create$', views.checkout_address_create, name="checkout_address"),
]
