# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-12-29 02:24
from __future__ import unicode_literals

from django.db import migrations, models
import sliders.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(blank=True, null=True, upload_to=sliders.models.upload_slider_image_path)),
                ('caption', models.CharField(blank=True, max_length=100, null=True)),
                ('sub_caption', models.CharField(blank=True, max_length=100, null=True)),
                ('button_url', models.URLField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Slider',
                'verbose_name_plural': 'Sliders',
            },
        ),
    ]
