import json
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.template import RequestContext
from django.http import JsonResponse
from django.forms.models import model_to_dict
from django.template.loader import render_to_string
from haystack.query import SearchQuerySet

from haystack.generic_views import FacetedSearchView as BaseFacetedSearchView
from rest_framework import generics
from rest_framework import permissions

from .models import Furniture, Category
from carts.models import Cart
from .serializers import CategorySerializer, FurnitureSerializer
from .forms import FacetedProductSearchForm


class CategoryListView(generics.ListAPIView):
    permission_classes = (permissions.AllowAny, )
    serializer_class = CategorySerializer
    queryset = Category.objects.root_nodes()


class FurnitureListView(generics.ListAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    serializer_class = FurnitureSerializer
    queryset = Furniture.objects.all()

def show_pagination(request, obj, num_of_page_to_show):
    paginator = Paginator(obj, num_of_page_to_show)
    page = request.GET.get('page')
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        object_list = paginator.page(1)
    except EmptyPage:
        object_list = paginator.page(paginator.num_pages)
    return object_list

def furnitures(request):
    furniture_list = Furniture.objects.all()
    categories = Category.objects.all()
    furnitures = show_pagination(request, furniture_list, 15)
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    context = {
        'furnitures': furnitures,
        'categories': categories,
        'cart': cart_obj
    }
    return render(request, 'furnitures/furnitures.html', context)

def ajax_furniture_detail(request):
    furniture_slug = request.GET.get('slug', None)
    qs = Furniture.objects.get(slug=furniture_slug)
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    context = {
    'furniture': model_to_dict(qs),
    'cart': model_to_dict(cart_obj),
    'status': 'ok'
    }
    rendered = render_to_string('furnitures/furniture_modal.html', context)
    return JsonResponse({'product_snippet': rendered})

def furniture(request, slug):
    instance = get_object_or_404(Furniture, slug = slug)
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    nodes = Category.objects.all()
    # nodes = Category.objects.add_related_count(Category.objects.all(), Furniture, 'category', 'o_count', True)
    context = {
        'furniture': instance,
        'cart': cart_obj,
        'nodes': nodes
    }
    return render(request, 'furnitures/furniture.html', context)

def show_product_on_category(request, hierarchy):
    category_slugs = hierarchy.split('/')
    categories = Category.objects.all()
    furnitures = None
    try:
        category = Category.objects.get(slug=category_slugs[-1]).get_descendants(include_self=True)
        furnitures = Furniture.objects.filter(category__in=category)
    except Category.DoesNotExist:
        print ('should show error in best possible way')
    except Furniture.DoesNotExist:
        furnitures = "No Furniture Exist In This Category"
    context = {
        'furnitures': furnitures,
        'categories': categories
    }
    return render(request, 'furnitures/furnitures.html', context)


def autocomplete(request):
    sqs = SearchQuerySet().autocomplete(
        content_auto=request.GET.get(
            'query',
            ''))[
        :10]
    s = []
    for result in sqs:
        d = {"value": result.name,
            "data": result.object.slug,
            "url": result.object.get_absolute_url()
            }
        s.append(d)
    output = {'suggestions': s}
    return JsonResponse(output)

class FacetedSearchView(BaseFacetedSearchView):

    form_class = FacetedProductSearchForm
    facet_fields = ['category', ]
    template_name = 'search/search_result.html'
    paginate_by = 3
    context_object_name = 'object_list'
