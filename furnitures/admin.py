from django.contrib import admin
from django.db import models

from markdownx.admin import MarkdownxModelAdmin
from markdownx.widgets import AdminMarkdownxWidget
from mptt.admin import MPTTModelAdmin

from .models import (Furniture, FurniturePic, Category,
    VariationCategory, FurnitureVariation, CustomDesign)


class CategoryAdmin(MPTTModelAdmin):
    list_display = ('name', 'slug')
    prepopulated_fields = {'slug': ('name',)}
    mptt_level_indent = 20

    class Meta:
        model = Category


class FurnitureAdmin(admin.ModelAdmin):
    list_display = ('name', 'category', 'timestamp')
    prepopulated_fields = {'slug': ('name',)}
    formfield_overrides = {
        models.TextField: {'widget': AdminMarkdownxWidget},
    }

    class Meta:
        model = Furniture

class VariationCategoryAdmin(admin.ModelAdmin):
    list_display = ('name', 'meta_info')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = VariationCategory

class FurnitureVariationAdmin(admin.ModelAdmin):
    list_display = ('variation_category', 'furniture', 'name', 'price')
    prepopulated_fields = {'slug': ('name',)}

    class Meta:
        model = FurnitureVariation

admin.site.register(Furniture, FurnitureAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(VariationCategory, VariationCategoryAdmin)
admin.site.register(FurnitureVariation, FurnitureVariationAdmin)
admin.site.register(CustomDesign)
admin.site.register(FurniturePic)
