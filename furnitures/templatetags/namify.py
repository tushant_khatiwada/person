from django import template

register = template.Library()

@register.filter
def get_name(value):
    spam = value.split('/')[-1] # bedroom-items/bed/queen-size-low-bed -> queen-size-low-bed
    spam = ' '.join(spam.split('-')) # queen-size-low-bed would be 2 queen size low bed
    return spam
