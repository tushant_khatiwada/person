from haystack import indexes
from furnitures.models import Furniture

class FurnitureIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.EdgeNgramField(document=True, use_template=True, template_name="search/indexes/furniture/furniture_text.txt")
    name = indexes.EdgeNgramField(model_attr='name')
    category = indexes.CharField(model_attr='category', faceted=True)
    content_auto = indexes.EdgeNgramField(model_attr='name')
    suggestions = indexes.FacetCharField()
    def get_model(self):
        return Furniture

    # def prepare_categories(self, obj):
    #     return [category.get_descendants().name for c in obj.category.all()]

    def index_queryset(self, using=None):
        return self.get_model().objects.all()


# for f in Furniture.objects.all():
    #print(f.category.get_ancestors(include_self=True))
