import random
import os

from django.db import models
from django.contrib.auth.models import User
from django.utils.text import slugify
from django.db.models.signals import pre_save, post_save
from django.core.urlresolvers import reverse

from mptt.models import MPTTModel, TreeForeignKey
from markdownx.models import MarkdownxField

from personalizedFurniture.utils import unique_slug_generator


class Manufacturer(models.Model):
	"""
	Represents a Manufacturer
	"""
	name = models.CharField(max_length=100, unique=True)
	slug = models.SlugField(max_length=100, unique=True)
	description = models.TextField(null=True, blank=True)
	is_active = models.BooleanField(default=True)
	updated_by = models.CharField(max_length=100)
	updated_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True)
	created_by = models.CharField(max_length=100)

	class Meta:
		ordering = ('name',)

	def __str__(self):
		return self.name

	# def get_absolute_url(self):
	#     return reverse('manufacturer-detail', kwargs={'slug': self.slug})

	def get_breadcrumbs(self):
		"""
		Returns name and url dictionary tuple
		"""
		return ({'name': self.name, 'url': self.get_absolute_url()},)

	@classmethod
	def get_manufacturers(cls):
		"""
		Returns list of active manufacturers
		"""
		return list(cls.objects.filter(is_active=True))

STATUS = (
	('approved', 'Approved'),
	('pending', 'Pending'),
	('rejected', 'Rejected'),
)

def get_filename_ext(filepath):
	base_name = os.path.basename(filepath)
	name, ext = os.path.splitext(base_name)
	return name, ext

def upload_image_path(instance, filename):
	new_filename = random.randint(1,3910209312)
	name, ext = get_filename_ext(filename)
	final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
	return "4nitures/{new_filename}/{final_filename}".format(
			new_filename=new_filename,
			final_filename=final_filename
			)


def upload_furniture_image_path(instance, filename):
	if (instance.parent):
		return '4niture/{0}/{1}'.format(instance.parent.name, instance.name)
	else:
		return '4niture/{0}'.format(instance.name)


def furniture_type_image_path(instance, filename):
	return '4niture/{0}/{1}'.format(instance.name, instance.name)


class Category(MPTTModel):
	name = models.CharField(max_length=100, blank=True, null=True)
	image = models.ImageField(null=True, blank=True,
							  upload_to=upload_furniture_image_path)
	slug = models.SlugField(max_length=200, unique=True)
	parent = TreeForeignKey('self', null=True, blank=True,
							related_name='children', db_index=True)

	class Meta:
		unique_together = (('parent', 'slug',))
		verbose_name_plural = 'Categories'

	class MPTTMeta:
		order_insertion_by = ['name']

	def __str__(self):
		return self.name

	def get_furniture_list(self):
		return Furniture.objects.filter(category=self.slug)

	def get_absolute_url(self):
		return '/'.join([x['slug'] for x in self.get_ancestors(include_self=True).values()])

	def is_second_node(self):
		return True if (self.get_ancestors().count() == 1) else False

	def get_slug_list_for_categories(self):
		try:
			ancestors = self.get_ancestors(include_self=True)
		except:
			ancestors = []
		else:
			ancestors = [i.slug for i in ancestors]
		slugs = []
		for i in range(len(ancestors)):
			slugs.append('/'.join(ancestors[:i + 1]))
		return slugs

	def get_count_children(self):
		return self.get_descendant_count()

class FurnitureQuerySet(models.QuerySet):
	def active(self):
		return self.filter(is_active=True)

	def featured(self):
		return self.active().filter(is_featured=True)

	def new_arrival(self, max_furnitures):
		return self.active().order_by('-id')[:max_furnitures]

	def free_shipping(self, max_furnitures):
		return self.active().filter(is_free_shipping=True)[:max_furnitures]

class FurnitureManager(models.Manager):
	def get_queryset(self):
		return FurnitureQuerySet(self.model, using=self._db)

	def all(self):
		return self.get_queryset().active()

	def featured(self):
		return self.get_queryset().featured()

	def new_arrival(self, max_furnitures):
		return self.get_queryset().new_arrival(max_furnitures)

	def free_shipping(self, max_furnitures):
		return self.get_queryset().free_shipping(max_furnitures)

	def get_by_id(self, slug):
		qs = self.get_queryset().filter(slug=slug)
		if qs.count() == 1:
			return qs.first()
		return None

class Furniture(models.Model):
	name = models.CharField(max_length=100, blank=True, null=True)
	manufacturer = models.ForeignKey(Manufacturer, blank=True, null=True)
	slug = models.SlugField(max_length=200, unique=True)
	sku = models.CharField(max_length=50, verbose_name='SKU', null=True, blank=True)
	gist = models.CharField(
		max_length=500, null=True, blank=True, help_text='Short description of the furniture')
	old_price = models.DecimalField(max_digits=20, decimal_places=2, default=0.0)
	price = models.DecimalField(decimal_places=2, max_digits=20, default=39.99)
	quantity = models.PositiveIntegerField(default="0", help_text='Stock quantity')
	content = MarkdownxField()
	note = models.CharField(max_length=500, null=True, blank=True, help_text="Notice on the product")
	meta_info = models.TextField(blank=True, null=True, help_text="Tips or some information related to the product")
	category = models.ForeignKey(Category, null=True, blank=True)
	is_active = models.BooleanField(
	default=True, help_text='Furniture is available for listing and sale')
	is_customizable = models.BooleanField(default=False)
	is_bestseller = models.BooleanField(
		default=False, help_text='It has been best seller')
	is_featured = models.BooleanField(
		default=False, help_text='Promote this furniture on main pages')
	is_free_shipping = models.BooleanField(
		default=False, help_text='No shipping charges')
	timestamp       = models.DateTimeField(auto_now_add=True)
	objects         = FurnitureManager()

	class Meta:
		verbose_name_plural = 'Furnitures'

	def __str__(self):
		return self.name

	def get_absolute_url(self):
		return reverse('furniture', kwargs={'slug': self.slug})

	def get_discount(self):
		"""
		Return discount on the furniture
		"""
		if self.old_price:
			return int(((self.old_price - self.price) / self.old_price) * 100)
		return 0

	def first_image(self):
		"""
		Return first image of the furniture
		"""
		if self.furniture_pics:
			print(self.furniture_pics.first().image.url)
			return self.furniture_pics.first().image.url
		return 'static/img/4niture.jpg'

	def furniture_total_price(self):
		return self.price * self.quantity

	@classmethod
	def get_active(cls):
		"""
		Returns active furnitures
		"""
		return cls.filter(is_active=True)

	@classmethod
	def featured_furnitures(cls):
		"""
		Returns featured furnitures
		"""
		return list(cls.get_active().filter(is_featured=True))

	@classmethod
	def recent_furnitures(cls, max_furnitures):
		"""
		Returns recent furnitures arrivals
		"""
		return list(cls.get_active().filter(is_active=True).order_by('-id')[:max_furnitures])

class FurniturePic(models.Model):
	"""
	Represents furniture picture
	"""
	furniture = models.ForeignKey(Furniture, related_name='furniture_pics')
	image = models.ImageField(upload_to=upload_image_path)
	display_order = models.IntegerField(default=0)
	created_on = models.DateTimeField(auto_now_add=True)

	class Meta:
		db_table = 'furnitures_furniture_pic'
		ordering = ('display_order', 'id')
		verbose_name_plural = 'Furniture Pics'

	def __str__(self):
		return '%s [Pic #id %s]' % (self.furniture.name, self.id)
#
#
# class WoodType(models.Model):
#     name = models.CharField(max_length=100, blank=True, null=True)
#     slug = models.SlugField(max_length=200, unique=True)
#     description = models.TextField(blank=True, null=True)
#     image = models.ImageField(null=True, blank=True,
#                               upload_to=furniture_type_image_path)
#     price = models.DecimalField(max_digits=6, decimal_places=2)
#
#     class Meta:
#         verbose_name = 'Type of Wood'
#         verbose_name_plural = 'Type of Woods'
#
#     def __str__(self):
#         return self.name
#
#
# class PolishType(models.Model):
#     name = models.CharField(max_length=100, blank=True, null=True)
#     slug = models.SlugField(max_length=200, unique=True)
#     description = models.TextField(blank=True, null=True)
#     image = models.ImageField(null=True, blank=True,
#                               upload_to=furniture_type_image_path)
#     price = models.DecimalField(max_digits=6, decimal_places=2)
#
#     class Meta:
#         verbose_name = 'Type of Polish'
#         verbose_name_plural = 'Type of Polishes'
#
#     def __str__(self):
#         return self.name
#
#
# class FabricType(models.Model):
#     name = models.CharField(max_length=100, blank=True, null=True)
#     slug = models.SlugField(max_length=200, unique=True)
#     description = models.TextField(blank=True, null=True)
#     image = models.ImageField(null=True, blank=True,
#                               upload_to=furniture_type_image_path)
#     price = models.DecimalField(max_digits=6, decimal_places=2)
#
#     class Meta:
#         verbose_name = 'Type of Fabric'
#         verbose_name_plural = 'Type of Fabrices'
#
#     def __str__(self):
#         return self.name


class CustomDesign(models.Model):
	image = models.ImageField(null=True, blank=True,
							  upload_to='4niture/custom_design/')
	description = models.TextField(blank=True, null=True)

	class Meta:
		verbose_name = 'Custom Design'
		verbose_name_plural = 'Custom Designs'

	def __str__(self):
		return self.image


class VariationCategory(models.Model):
	"""
	A category for furniture variation like Color, Wood Type, Polish Type
	"""
	name = models.CharField(max_length=100, blank=True, null=True)
	slug = models.SlugField(max_length=200, unique=True)
	meta_info = models.CharField(max_length=800, blank=True, null=True)
	updated_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Variation Category'
		verbose_name_plural = 'Variation Categories'

class FurnitureVariation(models.Model):
	"""
	Represents a variations/disparity for specific furniture like if Color is selected then
	the option for that color will be like Red, Green, Blue whatever a furniture
	owner provides
	"""
	variation_category = models.ForeignKey(VariationCategory, blank=True, null=True)
	furniture =  models.ForeignKey(Furniture, blank=True, null=True)
	name = models.CharField(max_length=100, blank=True, null=True)
	slug = models.SlugField(max_length=200, unique=True)
	price = models.DecimalField(max_digits=6, decimal_places=2)
	image = models.ImageField(null=True, blank=True, upload_to="4niture/variations/")
	meta_info = models.CharField(max_length=600, blank=True, null=True)
	notice = models.CharField(max_length=300, blank=True, null=True)
	updated_on = models.DateTimeField(auto_now=True)
	created_on = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return self.name

	class Meta:
		verbose_name = 'Furniture Variation'
		verbose_name_plural = 'Furniture Variations'


class CustomizeFurniture(models.Model):
	user = models.ForeignKey(User, blank=False, null=False)
	custom_design = models.ForeignKey(CustomDesign, blank=True, null=True)

	def __str__(self):
		return 'order by {0}'.format(self.user.username)

	class Meta:
		verbose_name = 'Customize Furniture'
		verbose_name_plural = 'Customize Furnitures'


def pre_save_slug(sender, instance, *args, **kwargs):
	if instance.slug == '':
		instance.slug = unique_slug_generator(instance)
pre_save.connect(pre_save_slug, sender=Furniture)
pre_save.connect(pre_save_slug, sender=VariationCategory)
pre_save.connect(pre_save_slug, sender=FurnitureVariation)
