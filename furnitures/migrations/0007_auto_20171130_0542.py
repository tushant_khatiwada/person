# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-11-30 05:42
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import furnitures.models


class Migration(migrations.Migration):

    dependencies = [
        ('furnitures', '0006_furniture_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='FurniturePic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.ImageField(upload_to=furnitures.models.upload_image_path)),
                ('display_order', models.IntegerField(default=0)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'verbose_name_plural': 'Furniture Pics',
                'db_table': 'furnitures_furniture_pic',
                'ordering': ('display_order', 'id'),
            },
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True)),
                ('slug', models.SlugField(max_length=100, unique=True)),
                ('description', models.TextField(blank=True, null=True)),
                ('is_active', models.BooleanField(default=True)),
                ('updated_by', models.CharField(max_length=100)),
                ('updated_on', models.DateTimeField(auto_now=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.CharField(max_length=100)),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.RemoveField(
            model_name='furniture',
            name='active',
        ),
        migrations.RemoveField(
            model_name='furniture',
            name='featured',
        ),
        migrations.RemoveField(
            model_name='furniture',
            name='image',
        ),
        migrations.AddField(
            model_name='furniture',
            name='gist',
            field=models.CharField(blank=True, help_text='Short description of the furniture', max_length=500, null=True),
        ),
        migrations.AddField(
            model_name='furniture',
            name='is_active',
            field=models.BooleanField(default=True, help_text='Furniture is available for listing and sale'),
        ),
        migrations.AddField(
            model_name='furniture',
            name='is_bestseller',
            field=models.BooleanField(default=False, help_text='It has been best seller'),
        ),
        migrations.AddField(
            model_name='furniture',
            name='is_featured',
            field=models.BooleanField(default=False, help_text='Promote this furniture on main pages'),
        ),
        migrations.AddField(
            model_name='furniture',
            name='is_free_shipping',
            field=models.BooleanField(default=False, help_text='No shipping charges'),
        ),
        migrations.AddField(
            model_name='furniture',
            name='old_price',
            field=models.DecimalField(decimal_places=2, default=0.0, max_digits=20),
        ),
        migrations.AddField(
            model_name='furniture',
            name='quantity',
            field=models.PositiveIntegerField(default='0', help_text='Stock quantity'),
        ),
        migrations.AddField(
            model_name='furniture',
            name='sku',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='SKU'),
        ),
        migrations.AddField(
            model_name='furniturepic',
            name='furniture',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='furniture_pics', to='furnitures.Furniture'),
        ),
    ]
