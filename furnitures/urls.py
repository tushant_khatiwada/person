from django.conf.urls import url

# from graphene_django.views import GraphQLView
from . import views

urlpatterns = [
    # url(r'^furnitures', views.FurnitureListView.as_view(), name="list-furnitures"),
    url(r'^furnitures', views.furnitures, name="furnitures"),
    url(r'^search/autocomplete/$', views.autocomplete, name="autocomplete"),
    url(r'^find/', views.FacetedSearchView.as_view(), name='haystack_search'),
    url(r'^furniture/(?P<slug>[\w-]+)', views.furniture, name="furniture"),
    url(r'^ajax/furniture', views.ajax_furniture_detail, name="ajax-furniture-detail"),
    url(r'^categories', views.CategoryListView.as_view(), name="list-categories"),
    url(r'^category/(?P<hierarchy>.+)', views.show_product_on_category, name='product-category'),
    # url(r'^graphql', GraphQLView.as_view(graphiql=True)),
]
