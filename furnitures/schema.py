from .models import Furniture, Category

import graphene
from graphene_django.fields import DjangoConnectionField
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType


class CategoryNode(DjangoObjectType):

    class Meta:
        model = Category
        # filter_fields = ['name', ]
        # interfaces = (Node, )


class FurnitureNode(DjangoObjectType):

    class Meta:
        model = Furniture
        # interfaces = (Node, )


class NewFurniture(graphene.Mutation):
    furniture = graphene.Field(lambda: FurnitureNode)

    class Arguments:
        name = graphene.String()
        content = graphene.String()
        category = graphene.String()

    # @classmethod
    def mutate(self, info, name, content, category):
        furniture = Furniture(name=name, content=content,
                              category=Category.objects.get(name=category))
        furniture.save()
        return NewFurniture(furniture=furniture)


class NewFurnitureMutation(graphene.ObjectType):
    new_furniture = NewFurniture.Field()
    # furniture = graphene.Field(FurnitureNode)


class Query(graphene.ObjectType):
    # category = Node.Field(CategoryNode)
    category = graphene.Field(
        CategoryNode, id=graphene.Int(), slug=graphene.String())
    all_categories = graphene.List(CategoryNode)

    def resolve_all_categories(self, info, **kwargs):
        return Category.objects.all()

    def resolve_category(self, info, **kwargs):
        cat_id = kwargs.get('id')
        slug = kwargs.get('slug')
        if cat_id is not None:
            return Category.objects.get(pk=cat_id)
        if slug is not None:
            return Category.objects.get(slug=slug)
        return None

    # furniture = Node.Field(FurnitureNode)
    all_furnitures = graphene.List(FurnitureNode)

    def resolve_all_furnitures(self, info, **kwargs):
        return Furniture.objects.all()
