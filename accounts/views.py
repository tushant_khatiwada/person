from django.shortcuts import render, redirect
from django.views.generic import CreateView
from django.utils.http import is_safe_url

from .models import GuestEmail
from .forms import GuestForm


class RequestFormAttachMixin(object):
    def get_form_kwargs(self):
        kwargs = super(RequestFormAttachMixin, self).get_form_kwargs()
        kwargs['request'] = self.request
        return kwargs

class NextUrlMixin(object):
    default_next = "/"
    def get_next_url(self):
        request = self.request
        next_ = request.GET.get('next')
        next_post = request.POST.get('next')
        redirect_path = next_ or next_post or None
        if is_safe_url(redirect_path, request.get_host()):
                return redirect_path
        return self.default_next

class GuestRegisterView(NextUrlMixin, RequestFormAttachMixin, CreateView):
	form_class = GuestForm
	default_next = '/accounts/signup/'

	def get_success_url(self):
		return self.get_next_url()

	def form_invalid(self, form):
		return redirect(self.default_next)
