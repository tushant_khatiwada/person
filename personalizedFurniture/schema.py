import graphene
import furnitures.schema

class Query(furnitures.schema.Query, graphene.ObjectType):
    pass

class RootMutation(furnitures.schema.NewFurnitureMutation, graphene.ObjectType):
    pass

schema = graphene.Schema(query=Query, mutation=RootMutation)
