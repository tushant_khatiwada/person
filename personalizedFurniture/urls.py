"""personalizedFurniture URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.decorators.csrf import csrf_exempt

# from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from graphene_django.views import GraphQLView

from accounts.views import GuestRegisterView
from .views import home

urlpatterns = [
    url(r'^$', home, name="home"),
    url(r'^', include('furnitures.urls')),
    url(r'^', include('addresses.urls')),
    url(r'^carts/', include('carts.urls')),
    url(r'^pages/', include('django.contrib.flatpages.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^register/guest$', GuestRegisterView.as_view(), name="guest_register"),
    url(r'^admin/', admin.site.urls),
    url(r'^markdownx/', include('markdownx.urls')),
    url(r'^graphql', csrf_exempt(GraphQLView.as_view(graphiql=True))),
    url(r'^api/v1/', include('furnitures.urls', namespace='furnitures-api')),
    url(r'^api/auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api/token/', obtain_jwt_token, name='api-token'),
    url(r'^api/token/refresh/', refresh_jwt_token),
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
