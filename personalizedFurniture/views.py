from django.shortcuts import render, get_object_or_404

from furnitures.models import Category, Furniture
from sliders.models import Slider


def home(request):
	categories = Category.objects.root_nodes()
	furnitures = Furniture.objects.new_arrival(20)
	featured_furnitures = Furniture.objects.featured()
	sliders = Slider.objects.all()
	context = {
		'categories': categories,
		'furnitures': furnitures,
		'featured': featured_furnitures,
		'sliders': sliders,
		}
	return render(request, 'home.html', context)
